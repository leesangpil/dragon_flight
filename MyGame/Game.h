#pragma once

#include "Sprite.h"
#include "Player.h"
#include "Enemy.h"
#include "Missile.h"
#include "Meteo.h"
#include "Coin.h"
#include "Object.h"
#include "Background.h"
//#include "Life.h"
#include "Item.h"
#include "Warning.h"
#include "Skill.h"
#include "Pumpkin.h"
#include "Blizzard.h"
#include "SkillBook.h"
#include "L_Potion.h"
#include "M_Potion.h"
#include "Smog.h"
#include "Font.h"
#include "Boss.h"
#include "BossMissile.h"
#include <vector>
#include <Time.h>

using namespace std;

class Game
{
public:
	Game()
	{
		key_count=0;
		distance=0;
		loop=0;
		Shoot_decision=0;
		skill_use=0;
		skill_count=0;
		score=0;
		Boss_make=0;
		mana=100;
		life=3;
		skill_level=1;
	}
	void Init();                //초기화 함수
	void Update(float dt);      //업데이트 함수(매 프레임 호출됨)
	void Exit();                // 종료 될때 호출되는 함수 

protected:
	bool CheckKey(int vKey);
	float ScreenWidth() const;
	float ScreenHeight() const;
	float ScreenCenterX() const;
	float ScreenCenterY() const;

private:
	void MakeBackground();
	void MoveBackground();
	void CreateMissile(int a);
	void MakeMissile();
	void ShootMissile();
	void MakeSmog(int j);
	void MakeEnemy();
	void MoveEnemy();
	void Missile_to_Dragon_Collision();
	void MakeMeteo();
	void MoveMeteo();
	void MakeCoin(int j);
	void MoveCoin();
	void MakeItem();
	void MoveItem();
	void MakeWarning();
	void MakeSkillName();
	void MoveSkillName();
	void MakeSkill();
	void MoveSkillLevel();
	void Print_Font();
	void Player_to_Coin_Collision();
	void MakeBoss();
	void Makebook();
	void Movebook();
	void Player_to_Book_Collision();
	void MakeM_Potion();
	void MakeL_Potion();
	void MoveL_Potion();
	void MoveM_Potion();
	void Remove_Smog();
	void ItoP_Collision();
	void MakeBossMissile();
	void MoveBossMissile();
	void Boss_to_Missile_Collision();
	void PtoMP_Collision();
	void PtoLP_Collision();
	void Pumpkin_to_Dragon_Collision();
	void Pumpkin_to_Boss_Collision();
	void MakeBlizzard();
	void MoveBlizzard();
	void Blizzard_to_Boss_Collision();
	void BossMove();
	void Blizzard_to_Dragon_Collision();


	float m_time;
	int key_count;
	int loop;
	int distance;
	int score;
	int life;
	int skill_level;
	int Shoot_decision;
	int skill_use;
	float skill_count;
	int Boss_make;
	int mana;
	char string[10];
	
	Font font;
	Font font1;
	Font font2;
	Font font3;
	Font font4;
	Font font5;
	Font font6;
	Font font7;
	Font font8;
	Font font9;

	Player player_2;
	Warning _warning;
	vector<Background*> _background;
	vector<Boss*> _boss;
	vector<BossMissile*> _bossmissile;
	vector<Skill*> _skill;
	vector<Pumpkin*> _pumpkin;
	vector<Meteo*> _meteo;
	vector<Object*> _object;
	vector<Enemy*> _enemy;
	vector<Coin*> _coin;
	vector<Item*> _item;
	vector<SkillBook*> _book;
	vector<Smog*> _smog;
	vector<L_Potion*> L_potion;
	vector<M_Potion*> M_potion;
	vector<Blizzard*> _blizzard;
};
