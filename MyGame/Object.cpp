#include "Object.h"

void Object::Remove()
{
	sprite->Clear();
}

void Object::SetImageFile(const char* fileName)
{
	sprite->SetImageFile(fileName);
}
void Object::SetX(float x)
{
	sprite->SetX(x);
}
void Object::SetY(float y)
{
	sprite->SetY(y);
}

float Object::GetX() const
{
	return sprite->GetX();
}
float Object::GetY() const
{
	return sprite->GetY();
}
void Object::SetPosition(float x,float y)
{
	sprite->SetPosition(x,y);
}
void Object::Move(float x,float y)
{
	sprite->Move(x,y);
}
void Object::SetScale(float fScale)
{
	Scale = fScale;

	sprite->SetScale(fScale);
}