//-------------------------------------------------------
// 유틸리티 함수
// 신경 쓰지 마시오.
//-------------------------------------------------------

bool Game::CheckKey(int vKey)
{
	return ( GetAsyncKeyState(vKey) & 0x8000 ) > 0 ;
}


// 화면 가로 크기
float Game::ScreenWidth() const
{
	cocos2d::CCSize visibleSize = cocos2d::CCDirector::sharedDirector()->getVisibleSize();
	return visibleSize.width;
}


//화면 세로 크기
float Game::ScreenHeight() const
{
	cocos2d::CCSize visibleSize = cocos2d::CCDirector::sharedDirector()->getVisibleSize();
	return visibleSize.height;
}

//화면 중간
float Game::ScreenCenterX() const
{
	return  ScreenWidth() * 0.5f;
}

//화면 중간
float Game::ScreenCenterY() const
{
	return  ScreenHeight() * 0.5f;
}


