#include "Font.h"



extern cocos2d::CCLayer	*g_pLayer;

Font::Font()
	:m_pFont(NULL) , m_x(0.0f) , m_y(0.0f) , m_bVisible(true)
{

}

Font::~Font()
{
	Clear();
}


void Font::SetFont(const char* fontName , float fontSize)
{
	if( m_pFont != NULL )
	{
		g_pLayer->removeChild(m_pFont,true);
	}

	m_pFont = cocos2d::CCLabelTTF::create("", fontName, fontSize);

	g_pLayer->addChild(m_pFont);
}


void Font::SetString(const char* str)
{
	if( m_pFont != NULL )
	{
		m_pFont->setString(str);
	}
}


void Font::SetX(float x)
{
	m_x = x;

	ApplyPosition();
}


void Font::SetY(float y)
{
	m_y = y;

	ApplyPosition();
}


void Font::SetPosition(float x,float y)
{
	m_x = x;
	m_y = y;

	ApplyPosition();
}


void Font::Move(float x,float y)
{
	m_x += x;
	m_y += y;

	ApplyPosition();
}


void Font::ApplyPosition()
{
	if( m_pFont == NULL )
		return;

	cocos2d::CCSize visibleSize = cocos2d::CCDirector::sharedDirector()->getVisibleSize();
    cocos2d::CCPoint origin = cocos2d::CCDirector::sharedDirector()->getVisibleOrigin();

	float rx = m_x + origin.x;
	float ry = m_y + origin.y;

	m_pFont->setPosition(ccp(rx , visibleSize.height -ry ));
}



void Font::Clear()
{
	if( g_pLayer != NULL && m_pFont != NULL )
	{
		g_pLayer->removeChild(m_pFont,true);
		m_pFont = NULL;
	}
}


void Font::SetScale(float fScale)
{
	m_fScale = fScale;

	m_pFont->setScale(fScale);
}
