#pragma once

#include "Object.h"

class Boss : public Object
{
public:

	Boss();
	~Boss();

	int GetHp();
	void SetHp(int hp);
	void Reduce_Hp();

	void SetPosition(float x, float y);

private:
	int hp;
	Sprite* spriteHP;
};