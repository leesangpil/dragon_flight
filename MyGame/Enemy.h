#pragma once

#include "Object.h"

class Enemy : public Object
{
public:
	Enemy();
	~Enemy();


	int GetHp();
	void SetHp(int hp);
	void Reduce_Hp();

	void EnemyHPset();
	void MoveEnemy();
protected:

private:

	int hp;
	Sprite *_sprite;
};