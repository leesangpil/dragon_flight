#include "Sprite.h"



cocos2d::CCLayer	*g_pLayer = NULL;

void SetGlobalLayer(cocos2d::CCLayer* layer)
{
	g_pLayer = layer;
}

Sprite::Sprite()
	:m_pSprite(NULL) , m_x(0.0f) , m_y(0.0f) , m_bVisible(true)
{

}

Sprite::~Sprite()
{
	Clear();
}


void Sprite::SetImageFile(const char* fileName)
{
	if( m_pSprite != NULL )
	{
		g_pLayer->removeChild(m_pSprite,true);
	}

	m_pSprite = cocos2d::CCSprite::create(fileName);

	g_pLayer->addChild(m_pSprite);

	ApplyPosition();
}


void Sprite::SetX(float x)
{
	m_x = x;

	ApplyPosition();
}


void Sprite::SetY(float y)
{
	m_y = y;

	ApplyPosition();
}


void Sprite::SetPosition(float x,float y)
{
	m_x = x;
	m_y = y;

	ApplyPosition();
}


void Sprite::Move(float x,float y)
{
	m_x += x;
	m_y += y;

	ApplyPosition();
}


void Sprite::ApplyPosition()
{
	if( m_pSprite == NULL )
		return;

	cocos2d::CCSize visibleSize = cocos2d::CCDirector::sharedDirector()->getVisibleSize();
    cocos2d::CCPoint origin = cocos2d::CCDirector::sharedDirector()->getVisibleOrigin();

	float rx = m_x + origin.x;
	float ry = m_y + origin.y;

	m_pSprite->setPosition(ccp(rx , visibleSize.height -ry ));
}



void Sprite::Clear()
{
	if( g_pLayer != NULL && m_pSprite != NULL )
	{
		g_pLayer->removeChild(m_pSprite,true);
		m_pSprite = NULL;
	}
}


void Sprite::SetScale(float fScale)
{
	m_fScale = fScale;

	m_pSprite->setScale(fScale);
}
