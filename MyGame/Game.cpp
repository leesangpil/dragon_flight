#include "Game.h"
#include "GameUtil.h"
//게임 다듬기

//초기화 함수
//게임 시작시 처음으로 한번 호출됨
void Game::Init()
{
	srand((unsigned)time(NULL));
	m_time = 0.0f;

	MakeBackground();
	player_2.SetImageFile("player3.png");
	player_2.SetPosition(300,700);
}
//배경 생성
void Game::MakeBackground()
{
	Background* _tempbackground;
	_tempbackground = new Background;
	_tempbackground->SetImageFile("배경1.png");
	_tempbackground->SetScale(1.3f);
	_tempbackground->SetPosition(300,385);
	_background.push_back(_tempbackground);
}
//배경 이동
void Game::MoveBackground()
{
	if(_background.size()==NULL)
	{
		MakeBackground();
	}

	for (int i = 0; i < (int)_background.size(); i++)
	{
		_background[i]->SetY(_background[i]->GetY() + 1.0f);

		if(_background[i]->GetY() >= 800)
		{
			_background[i]->SetY(-45);
		}
	}
}
// 몬스터 생성
void Game::MakeEnemy()
{
	Enemy* _tempEmemy;
	int No[5] = {60,180,300,420,540};       
	char monsterName[5][12] = {{"dragon1.png"}, {"dragon2.png"}, {"dragon3.png"}, {"dragon4.png"}, {"dragon5.png"}};
	
	if(Boss_make==0)
	{
		if (loop % 200 == 0)
		{
			for (int i = 0; i < 5; i++)
			{	
				_tempEmemy = new Enemy;
				_tempEmemy->SetImageFile(monsterName[rand() % 5]);
				_tempEmemy->SetHp(rand() % 5 + 1);
				_tempEmemy->SetPosition(No[i], 0);
				_enemy.push_back(_tempEmemy);
			}
		}
		else
		{}
	}
}
//몬스터 이동
void Game::MoveEnemy()
{
	for (int i = 0; i < (int)_enemy.size(); i++)
	{		
		_enemy[i]->MoveEnemy();
		
		if(_enemy[i]->GetY() >= 800)
		{
			_enemy[i]->Remove();
			_enemy.erase(_enemy.begin() + i);
			i--;
		}
	}
}
// 아이템(더블 샷) 플레이어 충돌 판정 
void Game::ItoP_Collision()
{
	for(int i=0;i<(int)_item.size();i++)
	{
		if(
			player_2.GetY()-_item[i]->GetY() >= -50 && player_2.GetY() - _item[i]->GetY() <= 10 &&
			player_2.GetX()-_item[i]->GetX() >= -50 && player_2.GetX() - _item[i]->GetX() <= 50)
		{
			Shoot_decision=70;         
			_item[i]->Remove();
			_item.erase(_item.begin()+i);
			i--;
		}
	}
}
//미사일 생성
void Game::CreateMissile(int a)
{
	Object* tempObject;
	tempObject = new Object;
	tempObject->SetImageFile("missile4.png");
	tempObject->SetScale(1.5f);
	tempObject->SetPosition( player_2.GetX() + a, player_2.GetY() - 50 );
	_object.push_back(tempObject);
}
//더블샷 효과 및 미사일 생성
void Game::MakeMissile()
{
	ItoP_Collision();

	if (loop % 10 == 0)
	{
		if(Shoot_decision==0)  
		{
			CreateMissile(0);
		}
		else      
		{
			CreateMissile(60);
			CreateMissile(-60);
			Shoot_decision--;
		}
	}
}
// 미사일 이동
void Game::ShootMissile()
{
	for (int i = 0; i < (int)_object.size(); i++)
	{
		_object[i]->SetY((_object[i]->GetY()) - 10.0f);

		if(_object[i]->GetY() <= 0)
		{
			_object[i]->Remove();
			_object.erase(_object.begin() + i);
			i--;
		}
	}
}
// 미사일 적 충돌판정 
void Game::Missile_to_Dragon_Collision()
{
	for (int i=0;i<(int)_object.size();i++)
	{
		for (int j=0;j<(int)_enemy.size();j++)
		{
			if((((_object[i]->GetY())-(_enemy[j]->GetY())<94)&&((_object[i]->GetY())-(_enemy[j]->GetY())>-60))&&
				(((_object[i]->GetX()-60)<=(_enemy[j]->GetX()))&&((_object[i]->GetX()+60)>=(_enemy[j]->GetX()))))
			{
				_enemy[j]->Reduce_Hp();

				_enemy[j]->EnemyHPset();

				if(_enemy[j]->GetHp()==0)
				{
					score = score + 30; 
					_enemy[j]->Remove();

					MakeSmog(j);
					MakeCoin(j);			
				_enemy.erase(_enemy.begin()+j);
				j--;
				}
				_object[i]->Remove();
				_object.erase(_object.begin()+i);
				i--;
				break;
			}
		}
	}
}
//플레이어 & 동전 충돌
void Game::Player_to_Coin_Collision()
{
	for(int i=0;i<(int)_coin.size();i++)
	{
		if(
			player_2.GetY()-_coin[i]->GetY() >= -40 && player_2.GetY() - _coin[i]->GetY()  <= 20 &&
			player_2.GetX()-_coin[i]->GetX() >= -40 && player_2.GetX() - _coin[i]->GetX() <= 40)
		{
			_coin[i]->Remove();
			_coin.erase(_coin.begin() + i);
			i--;
			score = score + 10;
		}
	}
}
//연기 생성
void Game::MakeSmog(int j)
{
	Smog* tempsmog;
	tempsmog = new Smog;
	tempsmog->SetImageFile("연기.png");
	tempsmog->SetScale(1.5f);
	tempsmog->SetPosition(_enemy[j]->GetX(),_enemy[j]->GetY());
	_smog.push_back(tempsmog);
}
// 운석 생성
void Game::MakeMeteo()
{
	if (loop % 850 == 0 || loop % 2100 == 0 || loop % 2120 == 0 || loop % 2140 == 0 || loop % 2160 == 0 || loop % 2180 == 0)
	{
		Meteo* tempmeteo;
		tempmeteo = new Meteo;
		tempmeteo->SetImageFile("Meteo.png");
		tempmeteo->SetScale(2.3f);
		tempmeteo->SetPosition(rand()%10 *50,0);
		_meteo.push_back(tempmeteo);
	}
}
// 운석 이동
void Game::MoveMeteo()
{
	for (int i = 0; i < (int)_meteo.size(); i++)
	{
		_meteo[i]->SetY((_meteo[i]->GetY()) + 7.0f);

		if(_meteo[i]->GetY() >= 800)
		{
			_meteo[i]->Remove();
			_meteo.erase(_meteo.begin() + i);
			i--;
		}
	}
}
//동전 생성
void Game::MakeCoin(int j)
{
	Coin* tempcoin;
	tempcoin = new Coin;
	tempcoin->SetImageFile("Coin.png");
	tempcoin->SetScale(1.7f);
	tempcoin->SetPosition(_enemy[j]->GetX(),_enemy[j]->GetY());
	_coin.push_back(tempcoin);	
}
// 동전 이동
void Game::MoveCoin()
{
	for (int i = 0; i < (int)_coin.size(); i++)
	{
		_coin[i]->SetY((_coin[i]->GetY()) + 1.7f);

		if(_coin[i]->GetY() >= 800)
		{
			_coin[i]->Remove();
			_coin.erase(_coin.begin() + i);
			i--;
		}
	}
}
//아이템 생성(더블 샷)
void Game::MakeItem()
{
	if (loop % 999 == 0)
	{
		Item* temp_item;
		temp_item = new Item;
		temp_item->SetImageFile("doubleshot.png");
		temp_item->SetScale(1.2f);
		temp_item->SetPosition(rand()%10*50+50,0);
		_item.push_back(temp_item);
	}
}
//아이템 이동(더블 샷)
void Game::MoveItem()
{
	for (int i = 0; i < (int)_item.size(); i++)
	{
		_item[i]->SetY((_item[i]->GetY()) + 6.0f);

		if(_item[i]->GetY() >= 800)
		{
			_item[i]->Remove();
			_item.erase(_item.begin() + i);
			i--;
		}
	}
}
// 경고창 생성
void Game::MakeWarning()
{
	if (loop % 2040 == 0)
	{
		_warning.SetImageFile("warning.png");
		_warning.SetScale(2.5f);
		_warning.SetPosition(300,200);
	}
	if(loop % 2080 ==0)
	{
		_warning.Remove();
	}
}
//스킬 이름 생성
void Game::MakeSkillName()
{
	if(skill_level == 1)
	{
		Skill* temp_skill_name;
		temp_skill_name = new Skill;
		temp_skill_name->SetImageFile("할로윈파티.png");
		temp_skill_name->SetScale(1.1f);
		temp_skill_name->SetPosition(player_2.GetX() , player_2.GetY()-70);
		_skill.push_back(temp_skill_name);
	}
	if(skill_level == 2)
	{
		Skill* temp_skill_name;
		temp_skill_name = new Skill;
		temp_skill_name->SetImageFile("눈보라.png");
		temp_skill_name->SetScale(0.8f);
		temp_skill_name->SetPosition(player_2.GetX() , player_2.GetY()-90);
		_skill.push_back(temp_skill_name);
	}

}
//스킬 생성
void Game::MakeSkill()
{
	if(skill_level == 1)
	{
		Pumpkin* temp_pumpkin;
		temp_pumpkin = new Pumpkin;
		temp_pumpkin->SetImageFile("호박.png");
		temp_pumpkin->SetScale(1.5f);
		temp_pumpkin->SetPosition(300 , 100);
		_pumpkin.push_back(temp_pumpkin);
	}
	if(skill_level == 2)
	{
		MakeBlizzard();
	}
}
//스킬 이름 이동
void Game::MoveSkillName()
{
	for(int i=0;i<(int)_skill.size();i++)
	{
		_skill[i]->SetPosition(player_2.GetX() , player_2.GetY()-90);
	}
}
//스킬 시전
void Game::MoveSkillLevel()
{
	float x = cos(m_time *5.0f) * 150.0f;
	float y = sin(m_time *5.0f) * 150.0f;

	if(skill_level == 1)
	{
		for(int i=0;i<(int)_pumpkin.size();i++)
		{
			_pumpkin[i]->SetPosition(300 + x, 100 + y);
		}
	}
	if(skill_level == 2)
	{
		MoveBlizzard();
	}
}
//블리자드 생성
void Game::MakeBlizzard()
{
	Blizzard* temp_blizzard;
	temp_blizzard = new Blizzard;
	temp_blizzard->SetImageFile("blizzard.png");
	temp_blizzard->SetScale(0.2f);
	temp_blizzard->SetPosition(rand()%100, (rand()%1000)-400);
	_blizzard.push_back(temp_blizzard);
}
//블리자드 시전
void Game::MoveBlizzard()
{
	for(int i=0;i<(int)_blizzard.size();i++)
	{
		_blizzard[i]->SetX(_blizzard[i]->GetX() + 15.0f);
		_blizzard[i]->SetY(_blizzard[i]->GetY() + 10.0f);
		if(_blizzard[i]->GetX() == 601 || _blizzard[i]->GetY() == 769)
		{
			_blizzard[i]->Remove();
			_blizzard.erase(_blizzard.begin()+i);
			i--;
		}
	}
}
//스킬 북 생성
void Game::Makebook()
{
	SkillBook* temp_book;
	temp_book = new SkillBook;
	temp_book->SetImageFile("스킬북.png");
	temp_book->SetScale(1.5f);
	temp_book->SetPosition(rand()%10*50+50,0);
	_book.push_back(temp_book);
}
//스킬 북 이동
void Game::Movebook()
{
	for (int i = 0; i < (int)_book.size(); i++)
	{
		_book[i]->SetY((_book[i]->GetY()) + 6.0f);

		if(_book[i]->GetY() >= 800)
		{
			_book[i]->Remove();
			_book.erase(_book.begin() + i);
			i--;
		}
	}
}
//마나 포션 생성
void Game::MakeM_Potion()
{
	M_Potion* temp_potion;
	if (loop % 2000 == 0)
	{
		temp_potion = new M_Potion;
		temp_potion->SetImageFile("마나물약.png");
		temp_potion->SetScale(2.0f);
		temp_potion->SetPosition(rand()%10*50+50,0);
		M_potion.push_back(temp_potion);
	}
}
//플레이어 & 마나포션 충돌판정
void Game::PtoMP_Collision()
{
	for(int i=0;i<(int)M_potion.size();i++)
	{
		if(
			player_2.GetY()-M_potion[i]->GetY() >= -40 && player_2.GetY() - M_potion[i]->GetY() <= 20 &&
			player_2.GetX()-M_potion[i]->GetX() >= -40 && player_2.GetX() - M_potion[i]->GetX() <= 40)
		{
			mana = mana+50;
			M_potion[i]->Remove();
			M_potion.erase(M_potion.begin() + i);
			i--;
		}
	}
}
//라이프 포션 생성
void Game::MakeL_Potion()
{
	L_Potion* temp_potion;
	if (loop % 12000 == 0)
	{
		temp_potion = new L_Potion;
		temp_potion->SetImageFile("라이프물약.png");
		temp_potion->SetScale(2.0f);
		temp_potion->SetPosition(rand()%10*50+50,0);
		L_potion.push_back(temp_potion);
	}
}
//플레이어 & 라이프포션 충돌판정
void Game::PtoLP_Collision()
{
	for(int i=0;i<(int)L_potion.size();i++)
	{
		if(
			player_2.GetY()-L_potion[i]->GetY() >= -40 && player_2.GetY() - L_potion[i]->GetY() <= 20 &&
			player_2.GetX()-L_potion[i]->GetX() >= -40 && player_2.GetX() - L_potion[i]->GetX() <= 40)
		{
			life++;
			L_potion[i]->Remove();
			L_potion.erase(L_potion.begin() + i);
			i--;
		}
	}
}
//마나 포션 이동
void Game::MoveM_Potion()
{
	for (int i = 0; i < (int)M_potion.size(); i++)
	{
		M_potion[i]->SetY((M_potion[i]->GetY()) + 7.0f);

		if(M_potion[i]->GetY() >= 800)
		{
			M_potion[i]->Remove();
			M_potion.erase(M_potion.begin() + i);
			i--;
		}
	}
}
//라이프 포션 이동
void Game::MoveL_Potion()
{
	for (int j = 0; j < (int)L_potion.size(); j++)
	{
		L_potion[j]->SetY((L_potion[j]->GetY()) + 7.0f);

		if(L_potion[j]->GetY() >= 800)
		{
			L_potion[j]->Remove();
			L_potion.erase(L_potion.begin() + j);
			j--;
		}
	}
}
//플레이어 & 스킬 북 충돌판정
void Game::Player_to_Book_Collision()
{
	for(int i=0;i<(int)_book.size();i++)
	{
		if(
			player_2.GetY()-_book[i]->GetY() >= -50 && player_2.GetY() - _book[i]->GetY() <= 10 &&
			player_2.GetX()-_book[i]->GetX() >= -50 && player_2.GetX() - _book[i]->GetX() <= 50)
		{      
			skill_level++;
			_book[i]->Remove();
			_book.erase(_book.begin()+i);
			i--;
		}
	}
}
//호박 & 드래곤 충돌판정
void Game::Pumpkin_to_Dragon_Collision()
{
	for(int i=0;i<(int)_pumpkin.size();i++)
	{
		for(int j=0;j<(int)_enemy.size();j++)
		{
			if(sqrt((_pumpkin[i]->GetX()-_enemy[j]->GetX())*(_pumpkin[i]->GetX()-_enemy[j]->GetX())
			+(_pumpkin[i]->GetY()-_enemy[j]->GetY())*(_pumpkin[i]->GetY()-_enemy[j]->GetY()))<=120)
			{
				_enemy[j]->Reduce_Hp();
				_enemy[j]->EnemyHPset();
				if(_enemy[j]->GetHp()==0)
				{
					score = score + 30; //용 죽이면 점수 30점씩 증가
					_enemy[j]->Remove();

					//MakeSmog(j);
					MakeCoin(j);		
					_enemy.erase(_enemy.begin()+j);
					j--;
				}
				
			}
		}
	}
}
//출력
void Game::Print_Font()
{
	//거리 출력
	_itoa(distance,string,10);
	font.SetFont("Arial", 20);
	strcat(string," m");
    font.SetString(string);
	font.SetPosition(150, 640);
	font1.SetFont("Arial", 20);
	font1.SetString("Distance : ");
	font1.SetPosition(55,640);
	//점수 출력
	_itoa(score,string,10);
	font2.SetFont("Arial", 20);
	font2.SetString(string);
	font2.SetPosition(150, 660);
	font3.SetFont("Arial", 20);
	font3.SetString("Score : ");
	font3.SetPosition(44,660);
	//마나 출력
	_itoa(mana,string,10);
	font4.SetFont("Arial", 20);
    font4.SetString(string);
	font4.SetPosition(150, 680);
	font5.SetFont("Arial", 20);
	font5.SetString("Mana : ");
	font5.SetPosition(43,680);
	//라이프 출력
	_itoa(life,string,10);
	font6.SetFont("Arial", 20);
    font6.SetString(string);
	font6.SetPosition(150, 700);
	font7.SetFont("Arial", 20);
	font7.SetString("Life : ");
	font7.SetPosition(35, 700);
	//스킬 레벨 출력
	_itoa(skill_level,string,10);
	font8.SetFont("Arial", 20);
    font8.SetString(string);
	font8.SetPosition(150, 720);
	font9.SetFont("Arial", 20);
	font9.SetString("Skill Level : ");
	font9.SetPosition(65, 720);
}
//보스 생성
void Game::MakeBoss()
{
	Boss_make=1;

	Boss* temp_boss;
	temp_boss = new Boss;
	temp_boss->SetPosition(ScreenCenterX() , 100);
	_boss.push_back(temp_boss);
}
//보스 미사일 생성
void Game::MakeBossMissile()
{
	if(Boss_make==1)
	{
		if (loop % 70 == 0)
		{
			BossMissile* temp_bossmissile;
			temp_bossmissile = new BossMissile;
			temp_bossmissile->SetImageFile("보스미사일.png");
			temp_bossmissile->SetScale(2.0f);
			for(int i=0;i<(int)_boss.size();i++)
			{
				temp_bossmissile->SetPosition(_boss[i]->GetX() , _boss[i]->GetY() + 60);
			}
		_bossmissile.push_back(temp_bossmissile);
		}
	}
}
//보스 미사일 이동
void Game::MoveBossMissile()
{
	for (int j = 0; j < (int)_bossmissile.size(); j++)
	{
		_bossmissile[j]->SetY((_bossmissile[j]->GetY()) + 7.0f);

		if(_bossmissile[j]->GetY() >= 800)
		{
			_bossmissile[j]->Remove();
			_bossmissile.erase(_bossmissile.begin() + j);
			j--;
		}
	}
}
//보스 & 미사일 충돌 체크
void Game::Boss_to_Missile_Collision()
{
	for(int j=0;j<(int)_boss.size();j++)
	{
		for(int i=0;i<(int)_object.size();i++)
		{
			if(
				_boss[j]->GetY()-_object[i]->GetY() >= -50 && _boss[j]->GetY() - _object[i]->GetY() <= 10 &&
				_boss[j]->GetX()-_object[i]->GetX() >= -50 && _boss[j]->GetX() - _object[i]->GetX() <= 50)
			{
				_boss[j]->Reduce_Hp();


				if(_boss[j]->GetHp() == 0)
				{
					Makebook();
					score = score + 1000;

					for(int h=0;h<(int)_boss.size();h++)
					{
						_boss[h]->Remove();
					}
					Boss_make=0;
				}
				_object[i]->Remove();
				_object.erase(_object.begin()+i);
				i--;
			}
		}
	}
	if(Boss_make ==0)
	{
		_boss.clear();
	}
}
//호박 & 보스 충돌판정
void Game::Pumpkin_to_Boss_Collision()
{
	for(int i=0;i<(int)_pumpkin.size();i++)
	{
		for(int j=0;j<(int)_boss.size();j++)
		{
			if(sqrt((_pumpkin[i]->GetX()-_boss[j]->GetX())*(_pumpkin[i]->GetX()-_boss[j]->GetX())
			+(_pumpkin[i]->GetY()-_boss[j]->GetY())*(_pumpkin[i]->GetY()-_boss[j]->GetY()))<=120)
			{
				_boss[j]->Reduce_Hp();

				if(_boss[j]->GetHp() == 0)
				{
					Makebook();
					score = score + 1000;

					for(int h=0;h<(int)_boss.size();h++)
					{
						_boss[h]->Remove();
					}
					Boss_make=0;
				}
			}
		}
	}
	if(Boss_make ==0)
	{
		_boss.clear();
	}
}
//블리자드 & 보스 충돌 판정
void Game::Blizzard_to_Boss_Collision()
{
	for(int j=0;j<(int)_boss.size();j++)
	{
		for(int i=0;i<(int)_blizzard.size();i++)
		{
			if(	_boss[j]->GetY()-_blizzard[i]->GetY() >= -60 && _boss[j]->GetY() - _blizzard[i]->GetY() <= 60 &&
				_boss[j]->GetX()-_blizzard[i]->GetX() >= -60 && _boss[j]->GetX() - _blizzard[i]->GetX() <= 60	)
			{
				_boss[j]->Reduce_Hp();
				_blizzard[i]->Remove();
				_blizzard.erase(_blizzard.begin()+i);
				i--;

				if(_boss[j]->GetHp() == 0)
				{
					Makebook();
					score = score + 1000;
					for(int h=0;h<(int)_boss.size();h++)
					{
						_boss[h]->Remove();
					}
					Boss_make=0;
				}
			}
		}
	}
	if(Boss_make ==0)
	{
		_boss.clear();
	}
}
//블리자드 & 드래곤 충돌판정
void Game::Blizzard_to_Dragon_Collision()
{
	for(int j=0;j<(int)_enemy.size();j++)
	{
		for(int i=0;i<(int)_blizzard.size();i++)
		{
			if(	_enemy[j]->GetY()-_blizzard[i]->GetY() >= -60 && _enemy[j]->GetY() - _blizzard[i]->GetY() <= 60 &&
				_enemy[j]->GetX()-_blizzard[i]->GetX() >= -60 && _enemy[j]->GetX() - _blizzard[i]->GetX() <= 60	)
			{
				_enemy[j]->Reduce_Hp();
				_enemy[j]->EnemyHPset();

				_enemy[j]->SetImageFile("icedragon.png");
				_enemy[j]->SetScale(0.8f);

				if(_enemy[j]->GetHp()==0)
				{
					score = score + 30; 
					_enemy[j]->Remove();
					MakeCoin(j);			
					_enemy.erase(_enemy.begin()+j);
					j--;
				}

				_blizzard[i]->Remove();
				_blizzard.erase(_blizzard.begin()+i);
				i--;
				break;
			}
		}
	}
}
//보스 이동
void Game::BossMove()
{
	for(int i=0;i<(int)_boss.size();i++)
	{
		_boss[i]->SetPosition( ScreenCenterX() + cos(m_time * 1.80f)*300.0f , _boss[i]->GetY() );
	}
}
//연기 지우기
void Game::Remove_Smog()
{
	for(int k=0;k<(int)_smog.size();k++)
	{
		_smog[k]->Remove();
	}
	_smog.clear();
}
// 업데이트 함수(매 프레임 호출됨)
// dt = delta time : 이전 프레임에서 얼만큼 시간이 지났는지의 값
void Game::Update(float dt)
{	
	m_time += dt;
	float x = cos(m_time *5.0f) * 150.0f;
	float y = sin(m_time *5.0f) * 150.0f;
	loop++;
	distance++;
	
	// 왼쪽 화살표 키를 누르면
	if( CheckKey(VK_LEFT) == true)
	{
		if(player_2.GetX()>=70)
		{
			player_2.Move( -dt * 1000.0f , 0.0f );
		}
	}
	// 오른쪽 화살표 키를 누르면
	if( CheckKey(VK_RIGHT) == true)
	{
		if(player_2.GetX()<=530)
		{
			player_2.Move( dt * 1000.0f , 0.0f );
		}
	}
	// 스페이스 키를 누르면
	if( CheckKey(VK_SPACE) == true && mana>=20)
	{
		key_count++;
		skill_use=1;
	}
	else
	{
		key_count = 0;
	}

	if(key_count == 1)
	{
		if(skill_level == 1)
		{
			mana = mana-20; 
		}
		if(skill_level == 2)
		{
			mana = mana-30;
		}
	MakeSkill();    //스킬 생성
	MakeSkillName();//스킬 이름생성
	}
	
	if(skill_use == 1)
	{
		skill_count += dt; //(프레임 돌때마다 0.06씩 더해짐)
		//마법 지속시간이 끝나면
		if(skill_count >= 7)
		{
			skill_count = 0;
			skill_use = 0; 
			for(int i=0;i<(int)_skill.size();i++)
			{
				_skill[i]->Remove();
				_skill.erase(_skill.begin()+i);
				i--;
			}
			if(skill_level >=1)
			{
				for(int j=0;j<(int)_pumpkin.size();j++)
				{
					_pumpkin[j]->Remove();
					_pumpkin.erase(_pumpkin.begin()+j);
					j--;
				}
			}
			if(skill_level >=2)
			{
				for(int k=0;k<(int)_blizzard.size();k++)
				{
					_blizzard[k]->Remove();
				}
				_blizzard.clear();
			}
		}
		// 마법 지속시간 동안
		else
		{
			MoveSkillLevel();       //스킬 이동
			if(skill_level == 2)
			{
				MakeBlizzard();     //블리자드 생성
				if(loop%2 == 0)
				{
					Blizzard_to_Dragon_Collision(); //블리자드 & 드래곤 충돌판정
					Blizzard_to_Boss_Collision();   //블리자드 & 보스 충돌판정
				}
			}
			Pumpkin_to_Dragon_Collision();       //호박 & 드래곤 충돌판정
			if(loop % 5 ==0)
			{
				Pumpkin_to_Boss_Collision();  //호박 & 보스 충돌판정
			}
			MoveSkillName();        //스킬이름 이동
		}
	}
	MoveBackground();				//배경화면 이동
	Print_Font();					//점수 및 거리 출력
	MakeMissile();					//미사일 생성
	ShootMissile();					//미사일 발사
	MakeEnemy();					 //적 생성
	MoveEnemy();					//적 이동
	MakeMeteo();					//운석 생성
	MoveMeteo();					//운석 이동
	Movebook();						 //스킬북 이동
	Remove_Smog();					//연기 삭제
	Missile_to_Dragon_Collision();  //미사일 & 드래곤 충돌 판정
	MoveCoin();						//동전 이동
	Player_to_Coin_Collision();    //플레이어 & 동전 충돌 판정 
	MakeItem();						//아이템 생성
	MoveItem();						//아이템 이동
	MakeWarning();					//경고창 띄움
	MakeM_Potion();					//마나포션 생성
	MoveM_Potion();					//마나포션 이동
	PtoMP_Collision();				//플레이어 & 마나포션 충돌 판정
	MakeL_Potion();					//라이프포션 생성
	MoveL_Potion();					  //라이프포션 이동
	PtoLP_Collision();				 //플레이어 & 라이프포션 충돌 판정
	Player_to_Book_Collision();    //플레이어 & 스킬북 충돌 판정

	if(distance % 3000 ==0)
		MakeBoss();					//보스 생성

	MakeBossMissile();				//보스 미사일 생성
	MoveBossMissile();				//보스 미사일 이동
	Boss_to_Missile_Collision();   //보스 & 미사일 충돌체크
	BossMove();						//보스 이동

	//루프 카운트 초기화
	if(loop==2181)
	{
		loop=0;
	}
}
// 종료 될때 호출되는 함수 
void Game::Exit()
{
	for (int i = 0; i < (int)_meteo.size(); i++)
	{
		if (NULL != _meteo[i])
		{
			delete _meteo[i];
		}
	}
	_meteo.clear();

	for (int i = 0; i < (int)_object.size(); i++)
	{
		if (NULL != _object[i])
		{
			delete _object[i];
		}
	}
	_object.clear();

	for (int i = 0; i < (int)_enemy.size(); i++)
	{
		if (NULL != _enemy[i])
		{
			delete _enemy[i];
		}
	}
	_enemy.clear();

	for (int i = 0; i < (int)_coin.size(); i++)
	{
		if (NULL != _coin[i])
		{
			delete _coin[i];
		}
	}
	_coin.clear();

	for (int i = 0; i < (int)_item.size(); i++)
	{
		if (NULL != _item[i])
		{
			delete _item[i];
		}
	}
	_item.clear();

	for (int i = 0; i < (int)_smog.size(); i++)
	{
		if (NULL != _smog[i])
		{
			delete _smog[i];
		}
	}
	_smog.clear();

	for (int i = 0; i < (int)_book.size(); i++)
	{
		if (NULL != _book[i])
		{
			delete _book[i];
		}
	}
	_book.clear();

	for (int i = 0; i < (int)L_potion.size(); i++)
	{
		if (NULL != L_potion[i])
		{
			delete L_potion[i];
		}
	}
	L_potion.clear();

	for (int i = 0; i < (int)M_potion.size(); i++)
	{
		if (NULL != M_potion[i])
		{
			delete M_potion[i];
		}
	}
	M_potion.clear();

	for (int i = 0; i < (int)_pumpkin.size(); i++)
	{
		if (NULL != _pumpkin[i])
		{
			delete _pumpkin[i];
		}
	}
	_pumpkin.clear();

	for (int i = 0; i < (int)_skill.size(); i++)
	{
		if (NULL != _skill[i])
		{
			delete _skill[i];
		}
	}
	_skill.clear();

	for (int i = 0; i < (int)_boss.size(); i++)
	{
		if (NULL != _boss[i])
		{
			delete _boss[i];
		}
	}
	_boss.clear();
}