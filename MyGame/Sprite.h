#pragma once

#include "cocos2d.h"

class Sprite
{
public:

	Sprite();
	~Sprite();

	void 	SetImageFile(const char* fileName);			//이미지 파일 세팅
	void 	Clear();									//이미지 지워버림
	
	void 	SetPosition(float x,float y);				//위치 세팅
	void	Move(float x,float y);						//현재 위치에서 해당 값 만큼 이동
	void	SetX(float x);
	void	SetY(float y);
	
	float	GetX() const		{ return m_x; } 
	float	GetY() const		{ return m_y; }

	void	SetScale(float fScale=1.0f);				//비율 크기 조정
	float	GetScale() const	{ return m_fScale; }


private:
	void	ApplyPosition();


private:

	float	m_x;
	float	m_y;
	float	m_fScale;
	bool 	m_bVisible;

	cocos2d::CCSprite		*m_pSprite;

};
