#include "Enemy.h"


Enemy::Enemy()
{
	_sprite = new Sprite;
}
Enemy::~Enemy()
{
	delete _sprite;
}

void Enemy::SetHp(int hp)
{
	Enemy::hp = hp;
}

int Enemy::GetHp()
{
	return Enemy::hp;
}

void Enemy::Reduce_Hp()
{
	Enemy::hp--;
}

void Enemy::EnemyHPset()
{
	if (GetHp() == 4)
	{
		_sprite->SetImageFile("4Hp.png");
		_sprite->SetScale(0.2f);
	}
	else if (GetHp() == 3)
	{
		_sprite->SetImageFile("3Hp.png");
		_sprite->SetScale(0.2f);
	}
	else if (GetHp() == 2)
	{
		_sprite->SetImageFile("2Hp.png");
		_sprite->SetScale(0.2f);
	}
	else if (GetHp() == 1)
	{
		_sprite->SetImageFile("1Hp.png");
		_sprite->SetScale(0.2f);
	}
	else
	{
		_sprite->Clear();
		delete _sprite;
	}
}

void Enemy::MoveEnemy()
{
	(*this).SetY((*this).GetY()+3.0f);
	_sprite->SetPosition((*this).GetX(),(*this).GetY()+50);
}