#include "Boss.h"


Boss::Boss()
{
	spriteHP = new Sprite;
	spriteHP->SetImageFile("hp20.png");
	spriteHP->SetScale(0.5f);

	SetImageFile("Boss.png");
	SetScale(1.0f);
	SetHp(101);
}

Boss::~Boss()
{
	delete spriteHP;
}


void Boss::SetHp(int hp)
{
	Boss::hp = hp;

	int index = 0;

	if( GetHp() > 0 )
	{
		index = ( GetHp() / 5 );
	}

	if( index <= 1 ) 
	{
		index = 1;
	}

	char fileName[20];
	if(GetHp() > 0)
	{
	sprintf( fileName , "hp%02d.png" , index);

	spriteHP->SetImageFile(fileName);
	spriteHP->SetScale(0.5f);
	}
	else if(GetHp() == 0)
	{
		spriteHP->Clear();
	}

}

int Boss::GetHp()
{
	return Boss::hp;
}
void Boss::Reduce_Hp()
{
	SetHp( hp - 1 );
}

void Boss::SetPosition(float x, float y)
{
	__super::SetPosition(x,y);

	spriteHP->SetPosition(x, y + 100);

	if(GetHp() == 0)
		spriteHP->Clear();
}
