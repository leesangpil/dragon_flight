#pragma once

#include "Sprite.h"

class Object
{
public:
	Object()
	{
		sprite = new Sprite;
	}
	virtual ~Object()
	{
		delete sprite;
	}
	
	void SetImageFile(const char* fileName);
	void Remove();
	void SetX(float x);
	void SetY(float y);
	float GetX() const;
	float GetY() const;
	virtual void SetPosition(float x, float y);
	void Move(float x, float y);
	void SetScale(float fScale = 1.0f);



private:
	float x,y;
	float Scale;
	Sprite* sprite;
};