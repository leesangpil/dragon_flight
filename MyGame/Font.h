// 글자 출력 클래스
//
// 사용법:
//
//
//	font.SetFont("Arial", 20);
//	font.SetString("asdfasdf");
//	font.SetPosition( 100 , 100 );
//


#pragma once

#include "cocos2d.h"

class Font
{
public:

	Font();
	~Font();

private:
	Font(const Font& font) {}									// 복사 생성자 사용 불가 			
	Font& operator=(const Font&) {return *this;}				// 대입 연산자 사용 불가


public:
	void 	SetFont(const char* fontName, float fontSize);		// 폰트 세팅
	void	SetString(const char* str);							// 문자열 세팅
	void 	Clear();											
	
	void 	SetPosition(float x,float y);						// 위치 세팅
	void	Move(float x,float y);								// 현재 위치에서 해당 값 만큼 이동
	void	SetX(float x);
	void	SetY(float y);
	
	float	GetX() const		{ return m_x; } 
	float	GetY() const		{ return m_y; }

	void	SetScale(float fScale=1.0f);						// 비율 크기 조정
	float	GetScale() const	{ return m_fScale; }


private:
	void	ApplyPosition();


private:

	float	m_x;
	float	m_y;
	float	m_fScale;
	bool 	m_bVisible;

	cocos2d::CCLabelTTF		*m_pFont;

};
